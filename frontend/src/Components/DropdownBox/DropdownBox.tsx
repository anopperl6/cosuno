import React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import "./index.css";

type DropdownBoxProps = {
  specialties: string[];
  inputValue: string;
  setInputValue: (a: string) => void;
};
export const DropdownBox = ({
  specialties,
  inputValue,
  setInputValue,
}: DropdownBoxProps) => {
  return (
    <div>
      <Autocomplete
        inputValue={inputValue}
        onInputChange={(event, newInputValue) => {
          let inputValue = newInputValue;
          setInputValue(inputValue);
        }}
        id="controllable-states-demo"
        options={specialties}
        sx={{ width: 300 }}
        renderInput={(params) => <TextField {...params} label="Specialties" />}
      />
    </div>
  );
};
