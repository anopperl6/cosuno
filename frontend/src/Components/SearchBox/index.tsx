import * as React from "react";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
// import IconButton from "@mui/material/IconButton";
// import SearchIcon from "@mui/icons-material/Search";
// import { Divider } from "@mui/material";
// import CircularProgress from "@mui/material/CircularProgress";
// import CloseIcon from "@mui/icons-material/Close";
type SearchBoxProps = {
  ChangeSearchTerm: (e: string) => void;
  setSearchTerm: (e: string) => void;
  isLoading: boolean;
};

export default function SearchBox({
  ChangeSearchTerm,
  setSearchTerm,
  isLoading,
}: SearchBoxProps) {
  return (
    <Paper
      component="form"
      sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 400 }}
    >
      <InputBase
        style={{ height: "60px", fontSize: "20px" }}
        sx={{ ml: 2, flex: 1 }}
        size="medium"
        placeholder="Company Name"
        inputProps={{ "aria-label": "Company Name" }}
        onChange={(e) => ChangeSearchTerm(e.target.value)}
      />
      {/* <IconButton sx={{ p: "19px" }} aria-label="search">
        <CloseIcon onClick={() => setSearchTerm("")} />
      </IconButton> */}

      {/* <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
      <IconButton color="primary" sx={{ p: "10px" }} aria-label="directions">
        <DirectionsIcon />
      </IconButton> */}
    </Paper>
  );
}
