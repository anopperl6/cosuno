import Checkbox from "@mui/material/Checkbox";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { useState } from "react";
import { ListItemIcon, ListItemText } from "@mui/material";

type MultiCheackBoxProps = {
  specialties: string[];
  inputValue: string[];
  setInputValue: (a: string[]) => void;
};

export const MultiCheackBox = ({
  specialties,
  inputValue,
  setInputValue,
}: MultiCheackBoxProps) => {
  const [Menu, setMenu] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);

  const handleCheck = (event: any) => {
    if (event.target.checked) {
      const selectedArr = [...selected, event.target.value];

      setInputValue(selectedArr);
      setSelected(selectedArr);
    } else {
      const selectedArr = selected.filter(
        (item) => item !== event.target.value
      );
      setSelected(selected.filter((item) => item !== event.target.value));
      setInputValue(selectedArr);
    }
  };

  return (
    <div>
      <Button
        variant="contained"
        endIcon={Menu ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
        onClick={() => setMenu(!Menu)}
      >
        specialties
      </Button>

      <div
        style={{
          display: Menu ? "block" : "none",
        }}
      >
        {specialties.map((specialtie) => (
          <MenuItem key={specialtie} value={specialtie}>
            <ListItemIcon>
              <Checkbox
                //checked={selected.indexOf(specialtie) > -1}
                value={specialtie}
                onChange={handleCheck}
              />
            </ListItemIcon>
            <ListItemText primary={specialtie} />
          </MenuItem>
        ))}
      </div>
    </div>
  );
};
