import * as React from "react";
import { DataGrid, GridColDef, GridToolbar } from "@mui/x-data-grid";

type dataProp = {
  company_name: string;
  logo: string;
  Specialties: string[];
  City: string[];
};

type newdataProp = {
  company_name: string;
  logo: string;
  Specialties: string;
  City: string;
};

type DataTableProps = {
  columns: GridColDef[];
  data: dataProp[];
  inputValue: string[];
};

export default function DataTable({
  columns,
  data,
  inputValue,
}: DataTableProps) {
  const modifyData = (data: dataProp[]) => {
    const newdata: newdataProp[] = [];

    for (let obj of data) {
      let intersection = obj.Specialties.filter((x) => inputValue.includes(x));
      if (inputValue.length === 0 || intersection.length > 0)
        newdata.push({
          ...obj,
          Specialties: obj.Specialties.join(","),
          City: obj.City.join(","),
        });
    }

    return newdata;
  };

  return (
    <div style={{ height: 400, width: 1000 }}>
      <DataGrid
        rows={modifyData(data)}
        columns={columns}
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
  );
}
