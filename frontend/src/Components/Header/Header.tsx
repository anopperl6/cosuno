import React from "react";
import logo from "../Header/logo.svg";
import "./Header.css";
import Typography from "@mui/material/Typography";

export default function Header() {
  return (
    <header>
      <img
        src={logo}
        style={{ maxWidth: 160, margin: "20px" }}
        alt="farmerscut"
      />
      <div className="headerInfo">
        <Typography variant="h6" gutterBottom component="div">
          All Company Information
        </Typography>
      </div>
    </header>
  );
}
