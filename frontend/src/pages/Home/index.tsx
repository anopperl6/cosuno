import React, { useState } from "react";
import axios from "axios";
// import CircularProgress from "@mui/material/CircularProgress";

import "./index.css";
import SearchBox from "../../Components/SearchBox";
// import { DropdownBox } from "../../Components/DropdownBox/DropdownBox";
import DataTable from "../../Components/DataTable";
import Header from "../../Components/Header/Header";
import { MultiCheackBox } from "../../Components/MultiCheckBox";
export const Home = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [specialties, setSpecialties] = useState<string[]>([]);
  const [inputValue, setInputValue] = useState<string[]>([]);
  //
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [data, setData] = useState([]);

  const ChangeSearchTerm = async (value: string) => {
    setIsLoading(true);
    setSearchTerm(value);

    const url = "/api/compnay_info?compnay_name=" + value;
    try {
      const { data } = await axios.get(url);

      setData(data);
      setIsLoading(false);
      setError(false);
      // get unique Specialties
      let SpecialtiesOptions: string[] = [];
      for (let obj of data) {
        SpecialtiesOptions.push(obj.Specialties);
      }
      SpecialtiesOptions = SpecialtiesOptions.flat().filter(
        (x, i, a) => a.indexOf(x) === i
      );
      setSpecialties(SpecialtiesOptions);
    } catch (error) {
      setError(true);
    }
  };

  const columns = [
    { field: "company_name", headerName: "Company name", flex: 1 },
    { field: "logo", headerName: "Logo", flex: 1 },
    { field: "Specialties", headerName: "Specialties", flex: 1 },
    { field: "City", headerName: "City", flex: 1 },
  ];

  return (
    <div>
      <Header />
      <div className="homeStyle">
        <div className="SearchBox">
          <SearchBox
            ChangeSearchTerm={ChangeSearchTerm}
            setSearchTerm={setSearchTerm}
            isLoading={isLoading}
          />
        </div>

        <div className="isLoading">{isLoading && <div> Loading... </div>}</div>

        {error && <div className="Error">error...</div>}
      </div>

      {data.length > 0 && (
        <div className="mainCompo">
          <div className="dropdown">
            <div>
              {/* <DropdownBox
                specialties={specialties}
                inputValue={inputValue}
                setInputValue={setInputValue}
              /> */}
            </div>
            <div>
              <MultiCheackBox
                specialties={specialties}
                inputValue={inputValue}
                setInputValue={setInputValue}
              />
            </div>
          </div>
          <div className="datatable">
            <div>
              <DataTable
                columns={columns}
                data={data}
                inputValue={inputValue}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
