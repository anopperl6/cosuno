import express, { Application, Request, Response } from "express";
import data from "./data.json"

const app: Application = express();
const port = 5000
type company_infoT = {
    id: number;
    company_name: string;
    logo: string;
    Specialties: string[];
    City: string[];
}

app.get("/api/compnay_info", (req: Request, res: Response): void => {
    const company_name: string = req.query.compnay_name as string
    if (company_name) {
        const patternRegex: RegExp = new RegExp(`^${company_name}`);
        const company_info: company_infoT[] = data.filter(company_info => patternRegex.test(company_info.company_name))
        res.send(company_info)
    } else {
        res.send([])
    }
})

app.listen(port, () => {
    console.log(`server is listening on port ${port}`)
})
