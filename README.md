## COSUNO demo app


### Tech stack
- Node(Typescript)
- ReactJS(Typescript)
- Ngnix
- Docker
- Docker-Compose  

### Mode
- Development
- Production

### How to run it in development mode

1.
- run `git clone https://gitlab.com/anopperl6/cosuno.git`
- run `cd cosuno`

2. start backend
- run `cd cosuno/frontend/`
- run `npm install`
- run `npm run dev`

3. start frontend
- run `cd cosuno/frontend/`
- run `yarn install`
- run `yarn start`
  
4.
- open page http://localhost:3000/ in browser (local)
- or
- open page http://IP_address:3000/ in browser (remote)
> Note: please add remote host IP address in link

### How to run it in production mode
1. 
- run `git clone https://gitlab.com/anopperl6/cosuno.git`
- run `cd cosuno`
- run `docker-compose -f docker-compose-prod.yml up -d --build`

This step will build docker images and create containers
 
 2. 
- open page https://IP_address:4343/ in browser

> Note: please add host IP address with https in link


# Screenshot

### 1. home page

![Image](screenshot/Capture.PNG?raw=true "1")

### 2. shows company information in real time as the user is typing.

![Image](screenshot/Capture1.PNG?raw=true "1")


### 3. shows company information after selected particular speciality (checkboxes)

![Image](screenshot/Capture2.PNG?raw=true "1")